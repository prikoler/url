import Foundation

struct Result: Codable {
    let results: [Article]
}

struct Article: Codable {
    let title: String
    let abstract: String
    let imageUrl: String

    enum CodingKeys: String, CodingKey {
        case title
        case abstract
        case media
    }

    enum MediaKeys: String, CodingKey {
        case mediaMetadata = "media-metadata"
    }

    enum MediaMetadataKeys: String, CodingKey {
        case imageUrl = "url"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        abstract = try container.decode(String.self, forKey: .abstract)

        var mediaContainer = try container.nestedUnkeyedContainer(forKey: .media)

        var imageUrl: String?
        while !mediaContainer.isAtEnd {
            let mediaItemContainer = try mediaContainer.nestedContainer(keyedBy: MediaKeys.self)
            var mediaMetadataContainer = try mediaItemContainer.nestedUnkeyedContainer(forKey: .mediaMetadata)

            while !mediaMetadataContainer.isAtEnd {
                let mediaMetadataItemContainer = try mediaMetadataContainer.nestedContainer(keyedBy: MediaMetadataKeys.self)
                imageUrl = try mediaMetadataItemContainer.decode(String.self, forKey: .imageUrl)
            }
        }

        self.imageUrl = imageUrl ?? ""
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(title, forKey: .title)
        try container.encode(abstract, forKey: .abstract)

        var mediaContainer = container.nestedUnkeyedContainer(forKey: .media)

        var mediaItemContainer = mediaContainer.nestedContainer(keyedBy: MediaKeys.self)
        var mediaMetadataContainer = mediaItemContainer.nestedUnkeyedContainer(forKey: .mediaMetadata)

        var mediaMetadataItemContainer = mediaMetadataContainer.nestedContainer(keyedBy: MediaMetadataKeys.self)
        try mediaMetadataItemContainer.encode(imageUrl, forKey: .imageUrl)
    }
}
